"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Registry {
    constructor(provider) {
        this.provider = provider;
    }
    register(collections) {
        collections.forEach((data) => {
            if (data.singleton) {
                this.provider.singleton(data.name, data.factory);
            }
            else {
                this.provider.bind(data.name, data.factory);
            }
        });
    }
}
exports.Registry = Registry;
