import { IRegistry } from "./contracts/registry";
import { Provider } from "./provider";
export declare class Registry {
    private provider;
    constructor(provider: Provider);
    register(collections: IRegistry[]): void;
}
