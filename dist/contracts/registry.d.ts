import { Provider } from "../provider";
export interface IRegistry {
    name: string;
    factory: (provider: Provider) => any;
    singleton: boolean;
}
