"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ServiceType;
(function (ServiceType) {
    ServiceType[ServiceType["bind"] = 0] = "bind";
    ServiceType[ServiceType["singleton"] = 1] = "singleton";
})(ServiceType || (ServiceType = {}));
class Provider {
    constructor() {
        this.registry = {};
    }
    bind(namespace, factory) {
        this.registry[namespace] = { factory, type: ServiceType.bind };
    }
    singleton(namespace, factory) {
        this.registry[namespace] = {
            factory: factory(this),
            type: ServiceType.singleton,
        };
    }
    make(namespace) {
        switch (this.registry[namespace].type) {
            case ServiceType.singleton:
                return this.registry[namespace].factory;
            default:
                return this.registry[namespace].factory(this);
        }
    }
}
exports.Provider = Provider;
