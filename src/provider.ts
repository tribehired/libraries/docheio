enum ServiceType {
  bind,
  singleton,
}

export class Provider {
  private registry: any = {}
  public bind(namespace: string, factory: (provider: Provider) => any) {
    this.registry[namespace] = { factory, type: ServiceType.bind }
  }

  public singleton(namespace: string, factory: (provider: Provider) => any) {
    this.registry[namespace] = {
      factory: factory(this),
      type: ServiceType.singleton,
    }
  }

  public make(namespace: string): any {
    switch (this.registry[namespace].type) {
      case ServiceType.singleton:
        return this.registry[namespace].factory
      default:
        return this.registry[namespace].factory(this)
    }
  }
}
