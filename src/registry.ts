import { IRegistry } from "./contracts/registry"
import { Provider } from "./provider"

export class Registry {
  constructor(private provider: Provider) {}

  public register(collections: IRegistry[]) {
    collections.forEach((data: IRegistry) => {
      if (data.singleton) {
        this.provider.singleton(data.name, data.factory)
        return
      }

      this.provider.bind(data.name, data.factory)
      return
    })
  }
}
